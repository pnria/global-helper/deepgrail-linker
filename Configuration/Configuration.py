import os
from configparser import ConfigParser


def read_config():
    # Read configuration file
    path_current_directory = os.path.dirname(__file__)
    path_config_file = os.path.join(path_current_directory, 'config.ini')
    config = ConfigParser()
    config.read(path_config_file)

    return config

