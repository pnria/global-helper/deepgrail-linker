import torch
from torch.nn import Module
from torch.nn.functional import nll_loss
from Linker.atom_map import atom_map, atom_map_redux


class SinkhornLoss(Module):
    r"""
    Loss for the linker
    """
    def __init__(self):
        super(SinkhornLoss, self).__init__()

    def forward(self, predictions, truths):
        return sum(nll_loss(link.flatten(0, 1), perm.flatten(), reduction='mean', ignore_index=-1)
                   for link, perm in zip(predictions, truths.permute(1, 0, 2)))


def measure_accuracy(batch_true_links, axiom_links_pred):
    r"""
    batch_true_links : (atom_vocab_size, batch_size, max_atoms_in_one_cat) contains the index of the negative atoms
    axiom_links_pred : (atom_vocab_size, batch_size, max_atoms_in_one_cat) contains the index of the negative atoms
    """
    padding = -1
    batch_true_links = batch_true_links.permute(1, 0, 2)
    correct_links = torch.ones(axiom_links_pred.size())
    correct_links[axiom_links_pred != batch_true_links] = 0
    correct_links[batch_true_links == padding] = 1
    num_correct_links = correct_links.sum().item()
    num_masked_atoms = len(batch_true_links[batch_true_links == padding])

    # diviser par nombre de links
    return (num_correct_links - num_masked_atoms) / (
                axiom_links_pred.size()[0] * axiom_links_pred.size()[1] * axiom_links_pred.size()[2] - num_masked_atoms)
