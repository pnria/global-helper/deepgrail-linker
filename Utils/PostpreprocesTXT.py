import itertools
import os
import re

import numpy as np
import pandas as pd


# dr = /
# dl = \
#
# def sub_tree_word(word_with_data: str):
#     word = ""
#     if not word_with_data.startswith("GOAL:"):
#         s = word_with_data.split('|')
#         word = s[0]
#         tree = s[1]
#     else:
#         tree = word_with_data
#     return word, tree
#
#
# def sub_tree_line(line_with_data: str):
#     line_list = line_with_data.split()
#     sentence = ""
#     sub_trees = []
#     for word_with_data in line_list:
#         w, t = sub_tree_word(word_with_data)
#         sentence += ' ' + w
#         if t not in ["\\", "/", "let"] and len(t) > 0:
#             sub_trees.append([t])
#         """if ('ppp' in list(itertools.chain(*sub_trees))):
#             print(sentence)"""
#     return sentence, list(itertools.chain(*sub_trees))
#
#
# def Txt_to_csv(file_name: str, result_name):
#     file = open(file_name, "r", encoding="utf8")
#     text = file.readlines()
#     sub = [sub_tree_line(data) for data in text]
#     df = pd.DataFrame(data=sub, columns=['X', 'Y'])
#     df.to_csv("../Datasets/" + result_name + "_dataset_links.csv", mode='a', index=False, header=False)
#
# def Txt_to_csv_header(file_name: str, result_name):
#     file = open(file_name, "r", encoding="utf8")
#     text = file.readlines()
#     sub = [sub_tree_line(data) for data in text]
#     df = pd.DataFrame(data=sub, columns=['X', 'Y'])
#     df.to_csv("../Datasets/" + result_name + "_dataset_links.csv", index=False)
def normalize_word(orig_word):
    word = orig_word.lower()
    if (word is "["):
        word = "("
    if (word is "]"):
        word = ")"

    return word


def read_maxentdata(path):
    allwords = []
    allsuper = []
    for filename in os.listdir(path):
        file = os.path.join(path, filename)

        with open(file, 'r', encoding="UTF8") as f:
            superset = set()
            words = ""
            supertags = []
            for line in f:
                line = line.strip().split()
                length = len(line)
                for l in range(length):
                    item = line[l].split('|')
                    if len(item) > 1:
                        orig_word = item[0]
                        word = normalize_word(orig_word)
                        supertag = item[1]
                        superset.add(supertag)
                        # words +=  ' ' +(str(orig_word))
                        words += ' ' + (str(orig_word))
                        supertags.append(supertag)
                    else:
                        supertag = line[l]
                        superset.add(supertag)
                        supertags.append(supertag)
                allwords.append(words)
                allsuper.append(supertags)
                words = ""
                supertags = []

    X = np.asarray(allwords)
    Z = np.asarray(allsuper)
    return X, Z

Xg,Zg = read_maxentdata("gold")
Xs,Zs= read_maxentdata("silver")
data3 = pd.read_csv('../SuperTagger/Datasets/m2_dataset.csv')

dfs = pd.DataFrame(columns = ["X", "Y"])
dfs['X'] = Xs
dfs['Y'] = Zs

print(len(dfs['X']))

rs = pd.merge(dfs, data3, on="X",how="inner").reindex(dfs.index)
rs.drop('Y1', inplace=True, axis=1)
rs.drop('Y2', inplace=True, axis=1)
# rs.drop_duplicates()

rs.to_csv("../Datasets/silver_dataset_links.csv", index=False)

dfg = pd.DataFrame(columns = ["X", "Y"])

dfg['X'] = Xg
dfg['Y'] = Zg

rg = pd.merge(dfg, data3, on="X",how="inner").reindex(dfg.index)
rg.drop('Y1', inplace=True, axis=1)
rg.drop('Y2', inplace=True, axis=1)
# rg.drop_duplicates()

rg.to_csv("../Datasets/gold_dataset_links.csv", index=False)

data1 = pd.read_csv('../Datasets/gold_dataset_links.csv')
data2 = pd.read_csv('../Datasets/silver_dataset_links.csv')
df = pd.merge(data1, data2,how='outer')
df = df.drop_duplicates(subset=['X'])


#
df[:len(df)-1].to_csv("../Datasets/goldANDsilver_dataset_links.csv", index=False)

#
# import os
# i = 0
# path = "gold"
# for filename in os.listdir(path):
#     if i == 0:
#         Txt_to_csv_header(os.path.join(path, filename),path)
#     else :
#         Txt_to_csv(os.path.join(path, filename),path)
#     i+=1
#
# i = 0
# path = "silver"
# for filename in os.listdir(path):
#     if i == 0:
#         Txt_to_csv_header(os.path.join(path, filename),path)
#     else :
#         Txt_to_csv(os.path.join(path, filename),path)
#     i+=1
#
# # reading csv files
# data1 = pd.read_csv('../Datasets/gold_dataset_links.csv')
# data2 = pd.read_csv('../Datasets/silver_dataset_links.csv')
# data3 = pd.read_csv('../SuperTagger/Datasets/m2_dataset.csv')
#
# # using merge function by setting how='left'
# df = pd.merge(data1, data2,how='outer')

#
# df.to_csv("../Datasets/goldANDsilver_dataset_links.csv", index=False)
