# DeepGrail Linker

This repository contains a Python implementation of a Proof Net using TLGbank data.

This code was designed to work with the [DeepGrail Tagger](https://gitlab.irit.fr/pnria/global-helper/deepgrail_tagger). 
In this repository we only use the embedding of the word from the tagger and the tags from the dataset, but next step is to use the prediction of the tagger for the linking step.
 
## Usage

### Installation
Python 3.9.10 **(Warning don't use Python 3.10**+**)**

Clone the project locally.

### Libraries installation

Run the init.sh script or install the Tagger project under SuperTagger name. And upload the tagger.pt in the directory 'models'. (You may need to modify 'model_tagger' in train.py.)

### Structure

The structure should look like this : 
```
.
.
├── Configuration                    # Configuration
│   ├── Configuration.py             # Contains the function to execute for config
│   └── config.ini                   # contains parameters
├── find_config.py                   # auto-configurate datasets parameters (max length sentence etc) according to the dataset given
├── requirements.txt                 # librairies needed
├── Datasets                         # TLGbank data with links
├── SuperTagger                      # The Supertagger directory (that you need to install)
│    ├── Datasets                    # TLGbank data
│    ├── SuperTagger                 # Implementation of BertForTokenClassification
│    │   ├── SuperTagger.py          # Main class
│    │   └── Tagging_bert_model.py   # Bert model
│    ├── predict.py                  # Example of prediction for supertagger
│    └── train.py                    # Example of train for supertagger
├── Linker                           # The Linker directory
│    ├── ...
│    └── Linker.py                   # Linker class containing the neural network
├── models                           
│    └── supertagger.pt              # the pt file contaning the pretrained supertagger (you need to install it)    
├── Output                           # Directory where your linker models will be saved if checkpoint=True in train               
├── TensorBoard                      # Directory where the stats will be saved if tensorboard=True in train
└──  train.py                        # Example of train
```

### Dataset format

The sentences should be in a column "X", the links with '_x' postfix should be in a column "Y" and the categories in a column "Z".
For the links each atom_x goes with the one and only other atom_x in the sentence.

## Training

Launch train.py, if you look at it you can give another dataset file and another tagging model.

In train, if you use `checkpoint=True`, the model is automatically saved in a folder: Output/Training_XX-XX_XX-XX. It saves
after each epoch. Use `tensorboard=True` for log saving in folder TensorBoard. (`tensorboard --logdir=logs` for see logs)

## Predicting

For predict on your data you need to load a model (save with this code).

```
df = read_csv_pgbar(file_path,20)
texts = df['X'].tolist()
categories = df['Z'].tolist()

linker = Linker(tagging_model)
linker.load_weights("your/linker/path")

links = linker.predict_with_categories(texts[7], categories[7])
print(links)
```

The file ```postprocessing.py``` will allow you to draw the prediction. (limited sentence length otherwise it will be confusing) 

You can also use the function ```predict_without_categories``` which only needs the sentence.

## LICENSE

Copyright ou © ou Copr. CNRS, (18/07/2022)

Contributeurs : 
[de Pourtales Caroline](https://www.linkedin.com/in/caroline-de-pourtales/), [Rabault Julien](https://www.linkedin.com/in/julienrabault), Richard Moot

Ce logiciel est un programme informatique servant à établir un Proof Net depuis une phrase française. 

Ce logiciel est régi par la licence CeCILL-C soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-C telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-C, et que vous en avez accepté les
termes.
